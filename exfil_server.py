import argparse, _thread as thread, os, struct
from socket import *


# Saves a file to exfil_path/<path> and writes <data> to it
def save_file(path, data):
    relpath = os.path.relpath('{}/{}'.format(exfil_path, path))
    if not os.path.isdir(os.path.dirname(relpath)):
        os.makedirs(os.path.dirname(relpath))                       # Make intermediate dirs, if necessary
    with open(relpath, 'wb') as f:                                  # Open file in 'write binary' mode
        f.write(data)


# Reads a pkt and returns the file's path and its contents
def read_packet(pkt):
    path_len, data_len = struct.unpack_from('!QQ', pkt, offset=8)   # Read 16 bytes, skipping the first 8
    struct_fmt = '!{}s{}s'.format(path_len, data_len)               # Get format for unpacking strings
    path, data = struct.unpack_from(struct_fmt, pkt, offset=24)     # Read the bytes from the packet
    path = path.decode()                                            # Convert bytes to string for path
    return path, data


# Handle a single client connection
def handle_client(conn):
    try:
        subdir = conn.recv(1024).decode()   # Get the subdir the client wishes to write to
        if subdir == '{default}':
            subdir = 'tmp'                  # Default to the exfil_path/tmp directory
        print('[~] Saving data to \'{}\''.format(subdir))
        conn.sendall('Saving data to \'{}\''.format(subdir).encode())

        pkt = conn.recv(8)                                                                  # Get packet length from first field
        while len(pkt) != 0:
            pkt_len = struct.unpack_from('!Q', pkt)[0]
            while len(pkt) < pkt_len:
                pkt += conn.recv(min(4096, pkt_len-len(pkt)))                               # Ensure all data is received
            path, data = read_packet(pkt)                                                   # Get relevant data
            path = os.path.relpath('{}/{}'.format(subdir, path))
            save_file(path, data)                                                           # Save the file
            print('\033[32m[+] Saved {} bytes to \'{}\'\033[0m'.format(len(data), path))
            conn.sendall('OK\n'.encode())
            pkt = conn.recv(8)

        conn.sendall('Goodbye.'.encode())
        print('\033[31m[-] Connection terminated.\033[0m')
        conn.close()
    except ConnectionResetError:
        print('\033[31m[-] Connection reset by peer.\033[0m')


# Dispatch client connections to threads
def dispatcher(sck):
    try:
        while True:
            conn, addr = sck.accept()                                               # Accept a connection
            print('\033[32m[+] Received connection from {}:{}\033[0m'.format(addr[0], addr[1]))
            thread.start_new_thread(handle_client, (conn,))                         # Dispatch to a thread
    except KeyboardInterrupt:
        print('\nkilled.')                                                          # Terminate on ctrl+C gracefully


def main():
    # Set up command line parser. Receives arguments for exfil location, address and port to bind to.
    parser = argparse.ArgumentParser(description='Host exfil server')
    parser.add_argument('-d', type=str, default='./exfil/', dest='exfil_path', metavar='path', help='Location to store exfil')
    parser.add_argument('-a', type=str, default='127.0.0.1', dest='address', metavar='address', help='Address to host on')
    parser.add_argument('-p', type=int, default=50007, dest='port', metavar='port', help='Port to host on')
    args = parser.parse_args()

    if not os.path.isdir(args.exfil_path):
        os.makedirs(args.exfil_path)                                # Make dir if it doesn't already exist
    global exfil_path
    exfil_path = args.exfil_path
    print('[~] Storing data in {}'.format(args.exfil_path))
    print('[~] Starting server...')
    sck = socket(AF_INET, SOCK_STREAM)                              # Create the socket
    try:
        sck.bind((args.address, args.port))
        sck.listen(5)                                               # Allow a queue of 5 pending connections
        print('\033[32m[+] Listening on {}:{}\033[0m'.format(args.address, args.port))
        dispatcher(sck)                                             # Dispatch connections to separate threads (allows multiple simultaneous connections)
    except Exception as err:
        print('\033[31m[-] Encountered error: {}\033[0m'.format(err))

    sck.close()                                                     # Clean up socket


if __name__ == '__main__':
    main()
